<?php
/*
Plugin Name: GWWI Functionality
Plugin URI: http://sideways8.com
Description: Custom functionality for GWWWI WordPress website.
Author: Sideways8 Interactive, LLC
Version: 0.0.1
Author URI: http://sideways8.com
*/

/** Absolute path to plugin directory (with trailing slash). */
define( 'GWWI_PLUGIN_DIR', trailingslashit( __DIR__ ) );

/** Public URL to plugin directory (with trailing slash). */
define( 'GWWI_PLUGIN_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );

// Load composer assets
require_once __DIR__ . '/vendor/autoload.php';

// Initialize plugin.
add_action( 'init', function() {
    \GWWI\GWWI_Functionality::get_instance();
}, 0 );
