/**
 * classHistory object is imported from WordPress.
 */

(function($) {

    var input = $('select[name=_attendee_name]');

    var app = new Vue({

        el: '#class-history-report',

        data: function() {
            return {
                user_key: ''
            }
        },

        mounted: function() {
            this.user_key = input.val();
            input.on('change', function() {
                app.user_key = $(this).val();
            });
        },

        computed: {

            results: function() {
                if ( 'undefined' !== typeof( classHistory[this.user_key] ) ) {
                    return classHistory[this.user_key];
                }
                return [];
            }
        }
    });

})(jQuery);