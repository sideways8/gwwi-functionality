jQuery(function($) {

    $('#order_data .edit_purchase_order').click( function() {
      $(this).parent().find('span').hide();
      $(this).parent().find('input').show();
      $(this).parent().find('input').focus();
      $(this).hide();
    });

    $('#order_data .edit_payment_type').click( function() {
        $(this).parent().find('span').hide();
        $(this).parent().find('input').show();
        $(this).parent().find('.option-label').show();
        $(this).hide();
    });
});