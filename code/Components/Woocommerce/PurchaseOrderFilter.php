<?php
namespace GWWI\Components\Woocommerce;

class PurchaseOrderFilter
{
    public function __construct() {
	    add_filter( 'woocommerce_checkout_fields' , [$this, 'custom_override_checkout_fields'] );
	    add_action( 'woocommerce_checkout_update_order_meta', [$this, 'my_custom_checkout_field_update_order_meta'] );
	    add_action( 'woocommerce_admin_order_data_after_billing_address', [$this, 'custom_checkout_field_display_admin_order_meta'], 10, 1 );
	    add_action( 'save_post_shop_order', [$this, 'save_order_purchaseorder'], 10, 3 );
    }

    public function save_order_purchaseorder( $post_ID, $post, $update ) {
	    if ( ! empty( $_POST['purchase_order'] ) ) {
		    update_post_meta( $post_ID, '_purchase_order', sanitize_text_field( $_POST['purchase_order'] ) );
	    }
    }


	/**
	 * Add Purchase Order field to WooCommerce Checkout
	 */
	public function custom_override_checkout_fields( $fields ) {
		$fields['billing']['purchase_order'] = array(
			'label'    => __( 'Purchase Order', 'woocommerce' ),
//			'placeholder'   => _x('PO', 'placeholder', 'woocommerce'),
			'required' => false,
			'class'    => array( 'form-row-wide' ),
			'clear'    => true
		);

		//place new item after Company to position its display on checkout
		$fields['billing'] = $this->moveKeyBefore( $fields['billing'], 'purchase_order', 'billing_country' );

		return $fields;
	}


    /**
     * Save Purchase Order Meta
     */
	public function my_custom_checkout_field_update_order_meta( $order_id ) {
		if ( ! empty( $_POST['purchase_order'] ) ) {
			update_post_meta( $order_id, '_purchase_order', sanitize_text_field( $_POST['purchase_order'] ) );
		}
	}


	/**
	 * Display field value on the order edit page
	 */
	public function custom_checkout_field_display_admin_order_meta($order){
		echo '<p class="purchase_order_wrap"><strong>'.__('Purchase Order').':</strong>' .
		     '  <span>' . get_post_meta( $order->get_id(), '_purchase_order', true ) . '</span>' .
		     '  <input type="text" id="purchase_order" name="purchase_order" value="' .
		        get_post_meta( $order->get_id(), '_purchase_order', true ) . '">' .
			 '<a href="#" onclick="return false;" class="edit_purchase_order"></a></p>';
	}


	/**
	 * array_splice was killing the index, preventing the form from being built correctly and data being saved.
	 * This method manually updates the location; seems to work.
	 */
	public function moveKeyBefore(array $arr, string $key, string $before)
	{
		$keys = array_keys($arr);
		if (in_array($key, $keys) && in_array($before, $keys)) {
			$newArr = [];
			foreach ($arr as $idx => $value) {
				switch ($idx) {
					case $key :
						continue;
					case $before:
						$newArr[$key] = $arr[$key];
						$newArr[$before] = $value;
						break;
					default:
						$newArr[$idx] = $value;
						break;
				}
			}

			return $newArr;
		}

		return $arr;
	}
}