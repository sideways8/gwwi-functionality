<?php
namespace GWWI\Components\Woocommerce;

class PaidByCheckOrderStatus
{
    const KEY = 'wc-paid-by-check';

    const LABEL = 'Paid by Check';

    public function __construct() {
        add_action( 'init', [$this, 'register_post_status'] );
        add_filter( 'wc_order_statuses', [$this, 'add_to_order_statuses'] );
        //$this->save_post();
    }

    public function register_post_status() {
        register_post_status( static::KEY, [
            'label'                     => static::LABEL,
            'public'                    => true,
            'show_in_admin_status_list' => true,
            'show_in_admin_all_list'    => true,
            'exclude_from_search'       => false,
        ] );
    }

    public function add_to_order_statuses( $statuses = [] ) {
        $statuses[static::KEY] = static::LABEL;
        return $statuses;
    }

    /**
     * Automatically set the post order status to 'wc-paid-by-check' if a
     * _check_number value is present.
     */
//Changed from tracking Paid by Check in Order Status to separate Woo Payment Method field
//    public function save_post() {
//        if ( empty( $_POST['_check_number'] ) ) {
//            return;
//        }
//        $_POST['order_status'] = static::KEY;
//    }
}