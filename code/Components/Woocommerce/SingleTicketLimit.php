<?php
/**
 * Created by PhpStorm.
 * User: BHo
 * Date: 11/9/2018
 * Time: 8:33 PM
 */

namespace GWWI\Components\Woocommerce;


class SingleTicketLimit {

	public function __construct() {
	    add_action( 'save_post', [$this, 'save_post'] );
        add_action( 'tribe_tickets_ticket_added', [$this, 'on_ticket_added'] );
		add_filter( 'woocommerce_return_to_shop_redirect', [$this, 'wc_empty_cart_redirect_url'] );
	}

    /**
     * If the saved post is a ticket or event, update the ticket or the event's tickets.
     * @param $post_id
     */
    public function save_post( $post_id ) {

        // Is this post_id a ticket?
        if ( tribe_events_product_is_ticket( $post_id ) ) {
            $this->set_ticket_meta( $post_id );
        }

        // Is this an event?
        if ( tribe_is_event( $post_id ) ) {
            // Get tickets
            $tickets = \Tribe__Tickets__Tickets::get_event_tickets( $post_id );
            foreach ( $tickets as $ticket ) {
                $this->set_ticket_meta( $ticket->ID );
            }
        }
    }

    /**
     * When a ticket is created, loop through the event's tickets and update ticket meta.
     * @param $event_id
     */
    public function on_ticket_added( $event_id ) {
        $tickets = \Tribe__Tickets__Tickets::get_event_tickets( $event_id );
        foreach( $tickets as $ticket ) {
            $this->set_ticket_meta( $ticket->ID );
        }
    }

    /**
     * Desired default meta state for tickets.
     * @param $ticket_id
     */
    public function set_ticket_meta( $ticket_id ) {
        update_post_meta( $ticket_id, '_sold_individually', 'yes' );
        update_post_meta( $ticket_id, '_downloadable', 'yes' );
        update_post_meta( $ticket_id, '_virtual', 'yes' );
    }


	/**
	 * The Return to Shop page sends users to the WooCommerce Shop page. This can be configured
	 * in WooCommerce settings by choosing an existing page. We want the Events Calendar's Events
	 * page which does not exist as a Page in WordPress. Here we redirect to the URL.
	 * @return string
	 */
	function wc_empty_cart_redirect_url() {
		return '/events/';
	}

	/**
	 * One time update of existing tickets to set as Sold Individually
	 */
//    public function update_all_tickets() {
//        /**
//         * Update All Tickets' Sold Individually settings to true
//         */
//        if ( isset( $_GET['individual'] ) ) {
//            echo "Updating all tickets to Sold Individually";
//
//            $count   = 0;
//            $tickets = get_posts( [
//                'post_type'      => 'product',
//                'posts_per_page' => '-1',
//            ] );
//
//            foreach ( $tickets as $ticket ) {
//                //if product have event meta, update sold individually
//                if ( get_post_meta( $ticket->ID, '_tribe_wooticket_for_event', true ) ) {
//                    update_post_meta( $ticket->ID, '_sold_individually', 'yes' );
//                    $count ++;
//                }
//            }
//
//            echo $count . " tickets updated successfully";
//        }
//    }
}
