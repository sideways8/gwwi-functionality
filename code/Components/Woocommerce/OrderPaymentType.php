<?php
/**
 * Created by PhpStorm.
 * User: BHo
 * Date: 11/13/2018
 * Time: 2:30 PM
 */

namespace GWWI\Components\Woocommerce;


class OrderPaymentType {

	const PAYMENT_TYPES = [
	    "Credit Card"    => "Credit Card",
        "Check payments" => "Check",
        "Complimentary"  => "Complimentary" ];

	public function __construct() {
		add_action( 'woocommerce_admin_order_data_after_billing_address', [$this, 'custom_checkout_field_display_admin_payment_meta'], 10, 1 );

		add_action( 'save_post_shop_order', [$this, 'save_order_paymenttype'], 10, 3 );

//		add_action( 'init', [$this, 'update_all_orders'], 10 );
//      NOTE: this wasn't working in Production, ran manually instead; leaving here for reference
	}


	/**
	 * Display payment type on the order edit page
	 * @param $order
	 */
	public function custom_checkout_field_display_admin_payment_meta( $order ) {
		$order_payment_type = get_post_meta( $order->get_id(), '_payment_method_title', true );
		?>
		<div class="payment_type_wrap">
			<div class="payment_type_label">
				<strong> <?php echo __('Payment Type'); ?>:</strong>
			</div>

			<div class="payment_type_value">
                <span><?php echo $order_payment_type; ?> </span>
				<?php
                foreach( static::PAYMENT_TYPES as $value => $type ) : ?>
                    <input type="radio" name="payment_type" value="<?php echo $value; ?>"
                        <?php echo ( $value == $order_payment_type ) ? ' checked ' : '' ?>><span class="option-label"><?php echo $type; ?><br></span>
                <?php endforeach; ?>

            </div>
            <a href="#" onclick="return false;" class="edit_payment_type"></a>
        </div>
		<?php
	}


	/**
	 * Save payment type meta
	 * @param $post_ID
	 * @param $post
	 * @param $update
	 */
	public function save_order_paymenttype( $post_ID, $post, $update ) {
		if ( ! empty( $_POST['payment_type'] ) ) {
			update_post_meta( $post_ID, '_payment_method_title', sanitize_text_field( $_POST['payment_type'] ) );
		}
	}



	/**
	 * One time update of existing tickets to set as Sold Individually
	 */
//	public function update_all_orders() {
//		/**
//		 * Update All Tickets' Sold Individually settings to true
//		 */
//		if ( isset( $_GET['payment'] ) ) {
//			echo "<br><br><br>Updating all orders with status Check to status Completed<br>";
//
//			$count   = 0;
//			$orders = get_posts( [
//				'post_type'      => 'shop_order',
//				'post_status'    => 'any',
//				'posts_per_page' => '-1',
//			] );
//
//			echo "Total orders = " . count( $orders ) . '<br>';
//
//
//			foreach ( $orders as $order ) {
//
//			    $wc_order = wc_get_order( $order->ID );
//
//			    if($wc_order ) {
//
//				    $status = $wc_order->get_status();
//                    if('paid-by-check' == $status ) {
//                        //set status to complete
//                        $wc_order->set_status('completed', 'Updated from Paid by Check to Completed before switch to Payment Type field', true);
//
//                        //set payment method to check
//                        $wc_order->set_payment_method('cheque');
//                        $wc_order->set_payment_method_title('Check payments');
//                        $wc_order->save();
//
//	                    echo "updated order id " . $order->ID . "<br>";
//	                    $count++;
//                    }
//
//
//			    }
//
//			}
//
//			echo "<br>" . $count . " orders updated successfully";
//		}
//	}

}