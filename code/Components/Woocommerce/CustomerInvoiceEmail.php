<?php
namespace GWWI\Components\Woocommerce;

class CustomerInvoiceEmail
{
    const EMAIL_KEY = 'customer_invoice';

    public function __construct() {
        // Add info after the header.
        add_action( 'woocommerce_email_order_details', [$this, 'after_header'], 0, 4 );
        add_action( 'woocommerce_email_customer_details', [$this, 'after_customer_details'], 100, 4 );

        // Add Check Details, Hotel List PDF
	    add_action( 'woocommerce_email_after_order_table', [$this, 'after_order_table'], 10, 4 );

        /* Disable tickets emails for Woo */
        add_filter( 'tribe_tickets_plus_email_enabled', '__return_false', 999 );

        /* Remove the message 'You'll receive your tickets in another email' from the Woo Order email */
        add_filter( 'wootickets_email_message', '__return_empty_string', 999 );

        /* Add Rebecca to Customer Order emails (Invoice and On Hold Order emails) l*/
        add_filter( 'woocommerce_email_headers', [$this, 'modify_headers'], 10, 2 );

        add_action( 'woocommerce_order_status_completed', [$this, 'maybe_send_email_for_auto_completed_purchase'] );
    }

    public function maybe_send_email_for_auto_completed_purchase( $order_id ) {
        $order = wc_get_order( $order_id );
        $method = $order->get_payment_method();
        if ( 'stripe' !== $method ) {
            return;
        }
        // Instantiate Woocommerce mailer.
        if ( ! class_exists('WC_Email_Customer_Invoice' ) ) {
            wc()->mailer();
        }
        $emailer = new \WC_Email_Customer_Invoice();
        $emailer->trigger( $order_id );
    }

    public function modify_headers( $header, $id ) {

        if ( $id !== static::EMAIL_KEY && $id !== 'customer_on_hold_order' ) {
            return $header;
        }

        if ( defined( 'WP_SITEURL' ) && false !== strpos( WP_SITEURL, '.staging' ) ) {
            return $header;
        }

        $header .= "BCC: rmansour@gwwi.org";

        return $header;
    }

    public function after_header( $order, $sent_to_admin, $plain_text, $email ) {

        if ( static::EMAIL_KEY !== $email->id )  {
            return;
        }
        ?>
        <h2>Thank you for your order!</h2>

        <p>Your order details are shown below.</p>
        <?php
    }

    public function after_customer_details( \WC_Order $order, $sent_to_admin, $plain_text, $email ) {

        $venues = [];
        $order_id = $order->get_id();
        $order_items = $order->get_items();

        // Loop through order items, these should be event tickets.
        foreach( $order_items as $order_item ) {

            $product_id = $order_item->get_data()['product_id'];

            // Get all ticket attendees.
            $attendees = tribe_tickets_get_attendees( $product_id );

            // Filter attendees to just this order.
            $attendees = array_filter( $attendees, function( $attendee ) use ( $order_id ) {
                return $order_id == $attendee['order_id'];
            });

            if ( empty( $attendees ) ) {
                continue;
            }

            $event_ids = tribe_tickets_get_event_ids( $product_id );
            $venue_id = tribe_get_venue_id( $event_ids[0] );
            $venue_info = tribe_get_venue_details( $venue_id );
            $venue_name = tribe_get_venue( $venue_id );

            printf( '<h3>%s</h3>', get_the_title( $product_id ) );
            printf( '<h4>%s</h4>', $venue_name );
            printf( '<p>%s</p>', $venue_info['address'] );

            // Print each attendee for this ticket.
            $i = 1;
            foreach( $attendees as $attendee ) {

                $first_name = $attendee['attendee_meta']['attendees-first-name']['value'];
                $last_name = $attendee['attendee_meta']['attendees-last-name']['value'];
                $phone_number = $attendee['attendee_meta']['phone-number']['value'];
                $email_address = $attendee['attendee_meta']['email']['value'];

                echo '<p>';
                printf( '<strong>Attendee #%s</strong><br>', $i );
                printf( 'Name: %s %s<br>', $first_name, $last_name );
                printf( 'Phone: %s<br>', $phone_number );
                printf( 'Email: %s<br>', $email_address );
                echo '</p>';
                $i++;
            }
        }
    }

	public function after_order_table( \WC_Order $order, $sent_to_admin, $plain_text, $email ) {
        $is_carrollton = false;
		$order_id = $order->get_id();
		$order_items = $order->get_items();
		foreach($order_items as $item) {
			$product_id = $item->get_data()['product_id'];
			$event_ids = tribe_tickets_get_event_ids( $product_id );
			if( ( tribe_get_city($event_ids[0]) == 'Carrollton' ) && ( tribe_get_state( $event_ids[0] ) == 'GA' ) ) {
			    $is_carrollton = true;
            }
		}

		//only show check mail to address if payment me;thod is check
        if( 'cheque' == $order->get_payment_method() || 'check' == $order->get_payment_method() ) : ?>
        <h2>Mail Checks To:</h2>
        <div>
            Georgia Water & Wastewater Institute, Inc.<br>
            511 Stadium Drive<br>
            Carrollton, GA 30117
        </div>
        <br>
        <?php endif; //payment type is check

        //only show hotel list pdf if location is Carrollton, GA
        if( $is_carrollton ) : ?>
        <h2>Recommended Hotels</h2>
        <div>
            Download the recommended hotel list in PDF format below.
            <br>
            <a href="<?php echo get_stylesheet_directory_uri() . '/assets/pdf/hotel-list.pdf'; ?>" target="_blank">Download Hotel List</a>
        </div>
        <?php endif; //venue is in Carrollton, GA
    }
}