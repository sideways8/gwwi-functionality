<?php
namespace GWWI\Components\Woocommerce;

class WoocommerceComponent
{
    public function __construct() {
        new CheckNumberMetaBox;
        new CustomerInvoiceEmail;
//        new PaidByCheckOrderStatus;
        new WoocommerceFilters;
        new PurchaseOrderFilter;
        new SingleTicketLimit;
        new OrderPaymentType;
    }
}