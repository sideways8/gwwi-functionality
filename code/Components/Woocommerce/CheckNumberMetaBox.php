<?php
namespace GWWI\Components\Woocommerce;

class CheckNumberMetaBox {

    const ID = 'check-number-meta-box';

    const TITLE = 'Check Details';

    public function __construct() {

        // Define which post types we want to hook.
        $post_types = ['shop_order'];

        foreach ( $post_types as $post_type ) {
            add_action( 'add_meta_boxes_' . $post_type, [$this, 'register_meta_box'] );
            add_action( 'save_post_' . $post_type, [$this, 'save_post'] );
        }
    }

    public function register_meta_box() {
        add_meta_box( static::ID, static::TITLE, [$this, 'render_meta_box'] );
    }

    public function render_meta_box( \WP_Post $post ) {
        $check_number = get_post_meta( $post->ID, '_check_number', true );
        ?>
        <table class="form-table">
            <tbody>
            <tr>
                <th>
                    <label for="_check_number">
                        Check Number
                    </label>
                </th>
                <td>
                    <input class="widefat" value="<?= esc_attr( $check_number ); ?>" id="_check_number" name="_check_number">
                    <p>
                        <i>
                            If a check number is entered, the Payment Method will automatically
                            be set to <strong>Check payments</strong>.
                        </i>
                    </p>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
    }

    public function save_post( $post_id ) {
        $keys = [
            '_check_number'
        ];

        foreach ( $keys as $key ) {
            if ( isset( $_POST[$key] ) ) {
                $value = filter_var( $_POST[$key], FILTER_SANITIZE_STRING );
                update_post_meta( $post_id, $key, $value );

                if( $key == '_check_number' && !empty( $value ) ) {
	                //Update Payment Method to Check
	                update_post_meta( $post_id, '_payment_method', 'cheque' );

	                //Update Payment Method Title
                    //Because this field is controlled by input on page, update POST to prevent meta being overwritten by POST value
	                $_POST['payment_type'] = 'Check payments';
                }
            }
        }
    }
}