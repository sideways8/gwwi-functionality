<?php
namespace GWWI\Components\Woocommerce;

/**
 * We are expecting tickets to
 *
 * Class MakeTicketsVirtualDownloadable
 *
 * @package GWWI\Components\Woocommerce
 */
class MakeTicketsVirtualDownloadable
{
    /**
     * Loop through all products and flag as downloadable, virtual.
     */
    public function products() {

        $products = wc_get_products([
            'limit' => -1,
        ]);
        
        foreach( $products as $product ) {
            /** @var $product \WC_Product */
            $product->set_downloadable(true);
            $product->set_virtual( true );
            $product->save();
        }
    }
}