<?php
namespace GWWI\Components\Woocommerce;

class WoocommerceFilters
{
    public function __construct() {
        add_filter( 'gettext_with_context', [$this, 'filter_buy_now_text'], 999, 4 );
        //add_filter( 'woocommerce_quantity_input_args', [$this, 'set_default_number_seats'], 10, 2 );
        add_action( 'wp_footer', [$this, 'trigger_attendee_javascript'] );
        add_action( 'admin_enqueue_scripts', [$this, 'edit_attendee_css'] );
    }

    public function edit_attendee_css() {
        ?>
        <style>
            #attendee-data-editor-space .attendee-meta-row {
                max-height: 400px !important;
                overflow: hidden !important;
                overflow-y: scroll !important;
            }
        </style>
        <?php
    }

    public function filter_buy_now_text( $translation, $text, $context, $domain ) {
        if ( 'Buy Now!' !== $text ) {
            return $translation;
        }
        return 'Register Now!';
    }

    public function set_default_number_seats( $args, $product ) {
        if ( ! $args['max_value'] ) {
            return $args;
        }
        $args['min_value'] = 1;
        return $args;
    }

    public function trigger_attendee_javascript() {
        ?>
        <script>
            jQuery(document).ready(function($) {
                setTimeout(function() {
                    $('.quantity input[type=number]').trigger('change');
                }, 125 );
            });
        </script>
        <?php
    }
}