<?php
namespace GWWI\Components\Events;

class EventsCustomMetaBox
{
	const ID = 'gwwi-certificate-metabox';
	const TITLE = 'Printable Certificate';

	public function __construct() {

		// Define which post types we want to hook.
		$post_types = ['tribe_events'];

		foreach( $post_types as $post_type ) {
			add_action( 'add_meta_boxes_' . $post_type, [$this, 'register_meta_box'] );
			add_action( 'save_post_' . $post_type, [$this, 'save_post'] );
		}
	}
	public function register_meta_box() {
		add_meta_box( static::ID, static::TITLE, [$this, 'render_meta_box'], 'tribe_events', 'side' );
	}
	public function render_meta_box( \WP_Post $post ) {
		// set nonce
		wp_nonce_field(dirname(__FILE__), 'gwwi_event_meta');

		// display meta input
		$custom                         = get_post_custom( $post->ID );
		$instructor_name                = isset( $custom['instructor_name'][0] ) ? $custom['instructor_name'][0] : '';
		$instructor_title               = isset( $custom['instructor_title'][0] ) ? $custom['instructor_title'][0] : '';
		$instructor_approval            = isset( $custom['instructor_approval'][0] ) ? $custom['instructor_approval'][0] : '';
		$instructor_signature_img_id    = isset( $custom['instructor_signature_img_id'][0] ) ? $custom['instructor_signature_img_id'][0] : '';
		$instructor_signature_img_url   = wp_get_attachment_image_url( $instructor_signature_img_id, 'medium' );
		?>
        <h3>Instructor Details</h3>
        <div class="meta-field">
            <label for="instructor_name">Name</label>
            <div><input type="text" name="instructor_name" id="instructor_name" value="<?php echo $instructor_name; ?>" /></div>
        </div>
        <div class="meta-field">
            <label for="instructor_title">Title</label>
            <div><input type="text" name="instructor_title" id="instructor_title" value="<?php echo $instructor_title; ?>" /></div>
        </div>
        <div class="meta-field">
            <label for="instructor_approval">Approval</label>
            <div><input type="text" name="instructor_approval" id="instructor_approval" value="<?php echo $instructor_approval; ?>" /></div>
        </div>
        <div class="meta-field">
            <label for="instructor_signature_img_id">Signature</label>
            <div class="wp-media-picker" data-key="instructor_signature_img_id" data-id="<?php echo $instructor_signature_img_id; ?>" data-url="<?php echo $instructor_signature_img_url; ?>"></div>
        </div>
		<?php
	}
	public function save_post( $post_id ) {

		// Do nothing on autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		//if valid nonce, save
		if ( isset( $_POST['gwwi_event_meta'] ) && wp_verify_nonce( $_POST['gwwi_event_meta'], dirname( __FILE__ ) ) ) {
			global $post;
			update_post_meta( $post->ID, "instructor_name", sanitize_text_field( wp_unslash( $_POST["instructor_name"] ) ) );
			update_post_meta( $post->ID, "instructor_title", sanitize_text_field( wp_unslash( $_POST["instructor_title"] ) ) );
			update_post_meta( $post->ID, "instructor_approval", sanitize_text_field( wp_unslash( $_POST["instructor_approval"] ) ) );
			update_post_meta( $post->ID, "instructor_signature_img_id", sanitize_text_field( wp_unslash( $_POST["instructor_signature_img_id"] ) ) );
		}


//		$keys = [
//			'_some_option'
//		];
//		foreach( $keys as $key ) {
//			if ( isset( $_POST[$key] ) ) {
//				$value = filter_var( $_POST[$key], FILTER_SANITIZE_STRING );
//				update_post_meta( $post_id, $key, $value );
//			}
//		}
	}
}
