<?php
namespace GWWI\Components\Events;

class EventsAdminListTableFilters
{
    public function __construct() {
        $post_types = ['tribe_events'];
        foreach ( $post_types as $post_type ) {
            add_filter( "manage_{$post_type}_posts_columns", [$this, 'columns'] );
            add_filter( "manage_edit-{$post_type}_sortable_columns", [$this, 'sortable_columns'] );
            add_action( "manage_{$post_type}_posts_custom_column", [$this, 'column_content'], 10, 2 );
        }
    }

    public function columns( $columns ) {
        $columns['tickets'] = 'Tickets Sold';
        $columns['attendees'] = 'Attendees';
        return $columns;
    }

    public function sortable_columns( $columns ) {
        return $columns;
    }

    public function column_content( $column_name, $post_id ) {
        $url = admin_url() . '/edit.php?post_type=tribe_events&page=tickets-attendees&event_id=' . $post_id;
        switch ( $column_name ) {
            case 'attendees':
                $attendees = tribe_tickets_get_attendees( $post_id );
                $attendees = array_filter( $attendees, function( $data ) {
                    return ! in_array( $data['order_status'], ['refunded', 'canceled', ''] );
                });
                $count = count( $attendees );
                printf( '<a href="%s">%s</a>', $url, $count );
        }
    }
}