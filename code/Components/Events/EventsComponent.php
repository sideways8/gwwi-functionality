<?php
namespace GWWI\Components\Events;

class EventsComponent
{
    public function __construct() {
	    new EventsAdminListTableFilters;
	    new EventsCustomMetaBox;
    }
}