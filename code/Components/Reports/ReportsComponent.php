<?php
namespace GWWI\Components\Reports;

class ReportsComponent
{
    public function __construct() {
        // Settings page container
        new Admin\ReportsAdminPageContainer;

        // Reports
        new Admin\Tabs\ClassRosterReport;
        new Admin\Tabs\ClassHistoryReport;
        new Admin\Tabs\FinancialInformationReport;
    }
}