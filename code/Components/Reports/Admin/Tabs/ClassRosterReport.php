<?php
namespace GWWI\Components\Reports\Admin\Tabs;

class ClassRosterReport extends ReportSettingsPageTabAbstract
{
    public $key = 'class-roster-report';

    public $label = 'Class Roster';

    public function get_classes() {
        $query = new \WP_Query([
            'post_type' => 'tribe_events',
            'posts_per_page' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        ]);

        return $query->posts;
    }

    public function render() {
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $('select').select2();
            });
        </script>

        <form method="post">

            <table class="form-table">
                <tbody>
                <tr>
                    <th>Class</th>
                    <td>
                        <select name="_event_id" class="widefat">
                            <?php
                            foreach ( $this->get_classes() as $post ) {
                                $title = $post->post_title;
                                $date_format = 'F j, Y';
                                $start_date = tribe_get_start_date( $post->ID, false, $date_format );
                                $end_date = tribe_get_end_date( $post->ID, false, $date_format );

                                $date_string = $start_date;
                                if ( $end_date != $start_date ) {
                                    $date_string .= ' - ' . $end_date;
                                }
                                printf( '<option value="%s">%s</option>', $post->ID, "$title ($date_string)" );
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>Report Type</th>
                    <td>
                        <select name="_report_type" class="widefat">
                            <?php
                            $report_types = [
                                'Payment Status',
                                'Check In Status'
                            ];
                            foreach( $report_types as $type ) {
                                printf( '<option value="%s">%s</option>', $type, $type );
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th>Report Output</th>
                    <td>
                        <select name="_report_output" class="widefat">
                            <option value="CSV">CSV</option>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>

            <button type="submit" class="button button-primary">Generate Report</button>

        </form>
        <?php
    }

    public function save() {

        $event_id = $_POST['_event_id'];
        $report_type = $_POST['_report_type'];

        switch( $report_type )
        {
            case 'Payment Status':
                $this->get_payment_status_report( $event_id );
                break;

            case 'Check In Status':
                $this->get_check_in_status_report( $event_id );
                break;
        }
    }

    public function get_payment_status_report( $event_id ) {
        $attendees = \Tribe__Tickets__Tickets::get_event_attendees( $event_id );
        $this->output_csv( $attendees, 'Payment Status' );
    }

    public function get_check_in_status_report( $event_id ) {
        $attendees = \Tribe__Tickets__Tickets::get_event_attendees( $event_id );
        $this->output_csv( $attendees, 'Check In Status' );
    }

    /**
     * @param string $type The report type name.
     *
     * @return array
     */
    public function get_column_headers( $type = 'Payment Status' ) {

        if ( 'Payment Status' === $type ) {
            return [
                'Ticket',
                'First Name',
                'Last Name',
                'Company',
                'Order ID',
                'Order Status',
            ];
        }

        if ( 'Check In Status' === $type ) {
            return [
                'Ticket',
                'Check In',
                'First Name',
                'Last Name',
                'Company'
            ];
        }
    }

    public function get_attendee_meta( $item ) {

        $order = wc_get_order( $item['order_id'] );

        $attendee_meta = $item['attendee_meta'];

        // Prepare ticket meta.
        if ( empty( $attendee_meta ) ) {
            $ticket_meta = [
                'attendees-first-name' => $order->get_billing_first_name(),
                'attendees-last-name'  => $order->get_billing_last_name(),
                'company'              => $order->get_billing_company(),
                'phone-number'         => $order->get_billing_phone(),
                'email'                => $order->get_billing_email(),
                'home-address'         => $order->get_billing_address_1() . $order->get_billing_address_2(),
                'city-state-zip'       => $order->get_billing_city() . ' ' . $order->get_billing_state() . ' ' .
                                          $order->get_billing_postcode()
            ];
        } else {
            $ticket_meta = [
                'attendees-first-name' => $attendee_meta['attendees-first-name']['value'],
                'attendees-last-name'  => $attendee_meta['attendees-last-name']['value'],
                'company'              => $attendee_meta['company']['value'],
                'phone-number'         => $attendee_meta['phone-number']['value'],
                'email'                => $attendee_meta['email']['value'],
                'home-address'         => $attendee_meta['home-address']['value'],
                'city-state-zip'       => $attendee_meta['city-state-zip']['value']
            ];
        }

        return $ticket_meta;
    }

    /**
     * @param array $items Attendees
     * @param string $type
     */
    public function get_csv_rows( $items = [], $type = 'Payment Status' ) {

        if ( 'Payment Status' === $type ) {
            foreach ( $items as $item ) {
                $ticket_meta = $this->get_attendee_meta( $item );
                printf( '"%s",', $item['ticket_name'] );
                printf( '"%s",', $ticket_meta['attendees-first-name'] );
                printf( '"%s",', $ticket_meta['attendees-last-name'] );
                printf( '"%s",', $ticket_meta['company'] );
                printf( '"%s",', $item['order_id']  );
                printf( '"%s",', $item['order_status_label'] );
                echo "\n";
            }
        }

        if ( 'Check In Status' === $type ) {
            foreach ( $items as $item ) {
                $ticket_meta = $this->get_attendee_meta( $item );
                printf( '"%s",', $item['ticket_name'] );
                printf( '"%s",', $item['check_in'] );
                printf( '"%s",', $ticket_meta['attendees-first-name'] );
                printf( '"%s",', $ticket_meta['attendees-last-name'] );
                printf( '"%s",', $ticket_meta['company'] );
                echo "\n";
            }
        }
    }

    /**
     * @param array $items
     * @param string $type 'Payment Status'
     */
    public function output_csv( $items = [], $type = 'Payment Status' ) {

        header( 'Content-Type: text/csv; charset=utf-8' );
        header( 'Content-Disposition: attachment; filename=data.csv' );

        foreach ( $this->get_column_headers( $type ) as $header ) {
            printf( '"%s",', $header );
        }
        echo "\n";

        $this->get_csv_rows( $items, $type );
        exit;
    }
}