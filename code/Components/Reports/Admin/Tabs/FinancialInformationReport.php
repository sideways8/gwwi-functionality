<?php
namespace GWWI\Components\Reports\Admin\Tabs;

class FinancialInformationReport extends ReportSettingsPageTabAbstract
{
    public $key = 'unpaid-balances-report';

    public $label = 'Financial Information';

    public function render() {
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $('select').select2();
            });
        </script>

        <form method="post" id="unpaid-balances">

            <table class="form-table">
                <tbody>
                <tr>
                    <th>Start Date</th>
                    <td>
                        <input type="date" name="_start_date" class="widefat">
                    </td>
                </tr>

                <tr>
                    <th>End Date</th>
                    <td>
                        <input type="date" name="_end_date" class="widefat">
                    </td>
                </tr>

                <tr>
                    <th>Sort</th>
                    <td>
                        <select name="_sort">
                            <option value="ASC">Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>

            <button type="submit" class="button button-primary">Generate Report</button>

        </form>

        <script>
            jQuery(document).ready(function($) {
                var form = $('#unpaid-balances');
                form.on('submit', function(e) {
                    var start = $('input[name=_start_date]'),
                        end = $('input[name=_end_date]');
                    if ( start.val().length === 0 || end.val().length === 0 ) {
                        alert('Specify a start and end date.');
                        e.preventDefault();
                        return false;
                    }
                });
            });
        </script>
        <?php
    }

    /**
     * @var array An array of statuses to ignore.
     */
    public $paid_statuses = [
        //'wc-paid-by-check',
        //'complete'
    ];

    /**
     * @param array $args
     *
     * @return array
     */
    public function get_unpaid_orders( $args = [] ) {

        $args = wp_parse_args( $args, [
            'start' => '',
            'end' => '',
            'sort' => 'DESC',
        ]);

        // Get all orders within the specified date range.
        $query = new \WP_Query([
            'post_type' => 'shop_order',
            'posts_per_page' => -1,
            'post_status' => 'any',
            'date_query' => [
                'before' => $args['end'],
                'after' => $args['start']
            ],
            'orderby' => 'date',
            'order' => $args['sort']
        ]);

        // Exclude any completed/paid orders.
        //$posts = array_filter( $query->posts, function( $post ) {
        //    return ! in_array( $post->post_status, $this->paid_statuses );
        //});

        // Get orders.
        $orders = array_map( function( $post ) {
            return wc_get_order( $post );
        }, $query->posts );

        return $orders;
    }

    public function save() {

        $end = $_POST['_end_date'];
        $start = $_POST['_start_date'];
        $sort = $_POST['_sort'];

        $orders = $this->get_unpaid_orders([
            'start' => $start,
            'end' => $end,
            'sort' => $sort
        ]);

        $this->output_csv( $orders );
    }

    public function get_column_headers() {
        return [
            'Order ID',
            'Date Created',
            'Date Payment Processed',
            'Payment Type',
            'Status',
            'Cost',
            'Class Start Date',
            'Class End Date',
            'Class Name',
            'Student Last Name',
            'Student First Name',
            'Company Name',
            'Check Number',
            'Last 4 of CC',
            'Billing First Name',
            'Billing Last Name',
            'Billing Address',
            'Student Email',
            'Student Phone Number',
            'Subtotal By Day',
        ];
    }

    /**
     * Cached objects.
     * @var \WC_Order_Item_Product[]
     */
    protected $order_items = [];

    protected function get_order_items( \WC_Order $order ) {
        $order_id = $order->get_id();
        if ( ! isset( $this->order_items[$order_id] ) ) {
            $this->order_items[$order_id] = $order->get_items();
        }
        return $this->order_items[$order_id];
    }

    /**
     * @var array
     */
    protected $order_attendees = [];

    protected function get_order_attendees( $order_id ) {
        if ( ! isset( $this->order_attendees[$order_id] ) ) {
            $this->order_attendees[$order_id] = tribe_tickets_get_attendees( $order_id );
        }
        return $this->order_attendees[$order_id];
    }

    public function get_order_comments( $order_id ) {
        remove_filter( 'comments_clauses', ['WC_Comments', 'exclude_order_comments'] );
        $comments = get_comments( [
            'post_id' => $order_id,
            'orderby' => 'comment_ID',
            'order'   => 'DESC',
            'approve' => 'approve',
            'type'    => 'order_note',
        ] );
        add_filter( 'comments_clauses', ['WC_Comments', 'exclude_order_comments'] );
        return $comments;
    }

    public function get_csv_field( \WC_Order $order, $key ) {

        $order_id = $order->get_id();

        $date_format = 'm/d/Y';

        switch( $key ) {

            case 'Date Payment Processed':

                $status = $order->get_status();

                switch( $status ) {

                    // Get order notes, and look for 'to paid by check'. Use that date.
                    case 'paid-by-check':
                        $notes = $this->get_order_comments( $order_id );
                        foreach( $notes as $note ) {
                            $note = (array) $note;
                            $content = $note['comment_content'];
                            $content = strtolower( $content );
                            $search = 'to paid by check';
                            if ( false === strpos( $content, $search ) ) {
                                continue;
                            }
                            $date = $note['comment_date'];
                            $date = new \DateTime($date);
                            if ( ! $date ) {
                                continue;
                            }
                            return $date->format( $date_format );
                        }
                        break;

                    // Is credit card?
                    case 'processing':
                        if ( 'stripe' === $order->get_payment_method() ) {
                            $date = $order->get_date_paid();
                            if ( ! empty( $date ) ) {
                                return $date->format( $date_format );
                            }
                        }
                        break;
                }

                break;

            case 'Order ID':
                return $order_id;
                break;

            case 'Date Created':
                return $order->get_date_created()->date( $date_format );
                break;

            case 'Payment Type':
                return $order->get_payment_method_title();
                break;

            case 'Status':
                return ucfirst( $order->get_status() );
                break;

            case 'Cost':
                return html_entity_decode( trim( strip_tags( $order->get_formatted_order_total() ) ) );
                break;

            case 'Class Start Date':
                $order_items = $this->get_order_items( $order );
                foreach( $order_items as $order_item ) {
                    /** @var \WC_Order_Item_Product $order_item */
                    $product_id = $order_item->get_product_id();
                    if ( tribe_events_product_is_ticket( $product_id ) ) {
                        $event = tribe_events_get_ticket_event( $product_id );
                        $start = tribe_get_start_date( $event, false, $date_format );
                        return $start;
                    }
                }
                break;

            case 'Class End Date':
                $order_items = $this->get_order_items( $order );
                foreach ( $order_items as $order_item ) {
                    /** @var \WC_Order_Item_Product $order_item */
                    $product_id = $order_item->get_product_id();
                    if ( tribe_events_product_is_ticket( $product_id ) ) {
                        $event = tribe_events_get_ticket_event( $product_id );
                        $start = tribe_get_start_date( $event, false, $date_format );
                        $end   = tribe_get_end_date( $event, false, $date_format );
                        if ( $start == $end ) {
                            return $start;
                        }
                        return $end;
                    }
                }
                break;

            case 'Class Name':
                $order_items = $this->get_order_items( $order );
                foreach ( $order_items as $order_item ) {
                    /** @var \WC_Order_Item_Product $order_item */
                    $product_id = $order_item->get_product_id();
                    if ( tribe_events_product_is_ticket( $product_id ) ) {
                        $event = tribe_events_get_ticket_event( $product_id );
                        return $event->post_title;
                    }
                }
                break;

            case 'Student Last Name':
                $attendees = $this->get_order_attendees( $order->get_id() );
                foreach( $attendees as $attendee ) {
                    if ( ! empty( $attendee['attendee_meta'] ) ) {
                        return $attendee['attendee_meta']['attendees-last-name']['value'];
                    }
                }
                break;

            case 'Student First Name':
                $attendees = $this->get_order_attendees( $order->get_id() );
                foreach ( $attendees as $attendee ) {
                    if ( ! empty( $attendee['attendee_meta'] ) ) {
                        return $attendee['attendee_meta']['attendees-first-name']['value'];
                    }
                }
                break;

            case 'Company Name':
                $attendees = $this->get_order_attendees( $order->get_id() );
                foreach ( $attendees as $attendee ) {
                    if ( ! empty( $attendee['attendee_meta'] ) ) {
                        return $attendee['attendee_meta']['company']['value'];
                    }
                }
                break;

            case 'Check Number':
                return get_post_meta( $order_id, '_check_number', true );
                break;

            case 'Last 4 of CC':
                break;

            case 'Billing First Name':
                return $order->get_billing_first_name();
                break;

            case 'Billing Last Name':
                return $order->get_billing_last_name();
                break;

            case 'Billing Address':
                $street = trim( $order->get_billing_address_1() . ' ' . $order->get_billing_address_2() );
                $city = $order->get_billing_city();
                $state = $order->get_billing_state();
                $zip = $order->get_billing_postcode();
                return sprintf( '%s - %s %s, %s', $street, $city, $state, $zip );
                break;

            case 'Student Email':
                $attendees = $this->get_order_attendees( $order->get_id() );
                foreach ( $attendees as $attendee ) {
                    if ( ! empty( $attendee['attendee_meta'] ) ) {
                        return $attendee['attendee_meta']['email']['value'];
                    }
                }
                break;

            case 'Student Phone Number':
                $attendees = $this->get_order_attendees( $order->get_id() );
                foreach ( $attendees as $attendee ) {
                    if ( ! empty( $attendee['attendee_meta'] ) ) {
                        $phone = $attendee['attendee_meta']['phone-number']['value'];
                        return preg_replace( '/[^0-9]/', '', $phone );
                    }
                }
                break;

            case 'Subtotal By Day':
                break;
        }
    }

    public function get_csv_rows( $orders = [] ) {
        $rows = [];
        foreach( $orders as $order ) {
            $row = [];
            foreach ( $this->get_column_headers() as $key ) {
                $row[] = $this->get_csv_field( $order, $key );
            }
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * @param array $items
     */
    public function output_csv( $items = [] ) {

        $end = $_POST['_end_date'];
        $start = $_POST['_start_date'];

        $filename = "Financial Information {$start} - {$end}.csv";
        header( 'Content-Type: text/csv; charset=utf-8' );
        header( 'Content-Disposition: attachment; filename="' . $filename . '"' );

        foreach ( $this->get_column_headers() as $header ) {
            printf( '"%s",', $header );
        }
        echo "\n";

        $rows = $this->get_csv_rows( $items );
        foreach( $rows as $row ) {
            foreach( $row as $data ) {
                printf( '"%s",', str_replace( '"', '', $data ) );
            }
            echo "\n";
        }
        exit;
    }
}