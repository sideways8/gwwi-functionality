<?php
namespace GWWI\Components\Reports\Admin\Tabs;

class ClassHistoryReport extends ReportSettingsPageTabAbstract
{
    public $key = 'class-history';

    public $label = 'Class History';

    public function __construct() {
        parent::__construct();
        add_action( 'admin_enqueue_scripts', [$this, 'register_scripts'] );
    }

    public function register_scripts() {
        wp_register_script( 'vue', GWWI_PLUGIN_URL . 'assets/js/vue.min.js', [], false, true );
        wp_register_script( 'class-history-report', GWWI_PLUGIN_URL . 'assets/js/class-history-report.js', ['vue', 'jquery'], time(), true );
    }

    public function get_attendees() {
        global $wpdb;

        // Get all order IDs.
        $query = "SELECT ID FROM {$wpdb->posts} WHERE post_type = 'shop_order';";
        $results = $wpdb->get_results( $query );
        $order_ids = wp_list_pluck( $results, 'ID' );

        // Get all orders with ticket meta.
        $query = "SELECT * FROM {$wpdb->postmeta} WHERE post_id IN (%s) AND meta_key = '_tribe_tickets_meta'";
        $query = sprintf( $query, implode( ',', $order_ids ) );
        $results = $wpdb->get_results( $query );

        // Declare attendees array.
        $attendees = [];

        foreach( $results as $result ) {
            $values = unserialize( $result->meta_value );
            foreach( $values as $ticket_id => $items ) {
                foreach( $items as $item ) {
                    $item['order_id'] = $result->post_id;
                    $item['ticket_id'] = $ticket_id;
                }

            }
            $attendees[] = $item;
        }

        $attendees_grouped = [];

        foreach( $attendees as $data ) {

            // Get first name
            $first_name = '';
            if ( isset( $data['attendees-first-name'] ) ) {
                $first_name = $data['attendees-first-name'];
            }
            if ( isset( $data['first-name'] ) ) {
                $first_name = $data['first-name'];
            }

            // Get last name
            $last_name = '';
            if ( isset( $data['attendees-last-name'] ) ) {
                $last_name = $data['attendees-last-name'];
                $data['attendees-first-name'] = $first_name;
            }
            if ( isset( $data['last-name'] ) ) {
                $last_name = $data['last-name'];
                $data['attendees-last-name'] = $last_name;
            }

            $first_name = ucfirst( strtolower( trim( $first_name ) ) );
            $last_name = ucfirst( strtolower( trim( $last_name ) ) );
            $phone = ucfirst( strtolower( $data['phone-number'] ) );
            $phone = preg_replace('/\D/', '', $phone);
            $key = "$first_name $last_name | $phone";

            // Create an index.
            if ( ! isset( $attendees_grouped[$key] ) ) {
                $attendees_grouped[$key] = [];
            }

            // Push this item.
            $attendees_grouped[$key][] = $data;
        }

        return $attendees_grouped;
    }

    public function get_attendees_optimized() {
        $key = 'attendees-grouped';
        if ( false === $data = get_transient( $key ) ) {
            $data = $this->get_attendees();
            set_transient( $key, $data, 60 *60 );
        }
        return $data;
    }


    public function get_attendee_names_ordered() {
        $attendees = $this->get_attendees_optimized();
        $keys = array_keys( $attendees );
        sort($keys);
        return $keys;
    }

    public function render() {

        //$this->build_report();

        //wp_localize_script( 'class-history-report', 'classHistory', $this->get_report() );
        //wp_enqueue_script( 'class-history-report' );
        ?>

        <script>
            jQuery(document).ready(function($) {
                $('select').select2();
            });
        </script>

        <form method="post">

        <table class="form-table">
            <tbody>
            <tr>
                <th>Student</th>
                <td>
                    <select name="_attendee_name" class="widefat">
                    <?php
                    foreach( $this->get_attendee_names_ordered() as $key ) {
                        printf( '<option value="%s">%s</option>', esc_attr( $key ), $key );
                    }
                    ?>
                    </select>
                </td>
            </tr>

            <tr>
                <th>Report Output</th>
                <td>
                    <select name="_report_output" class="widefat">
                        <option value="CSV">CSV</option>
                        <option value="Web" selected="selected">Web</option>
                    </select>
                </td>
            </tr>

            </tbody>
        </table>

        <button type="submit" class="button button-primary">Generate Report</button>

        </form>

        <hr>

        <?php if ( ! empty( $this->results ) ) : ?>

        <table>
            <thead>
            <tr align="left">
                <th>Order</th>
                <th>Date</th>
                <th>User</th>
                <th>Event</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Company</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Order Status</th>
                <th>Payment Method</th>
                <th>Price</th>
                <th>Check Number</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach( $this->results as $row ) {
                echo '<tr>';

                // Order
                $order_id = $row['order_id'];
                $url = get_edit_post_link( $order_id );
                printf( '<td><a href="%s" target="_blank">%s</a></td>', $url, $order_id );

                // Date
                printf( '<td>%s</td>', $row['date_created'] );

                // User
                $user_id = $row['user_id'];
                if ( ! empty( $user_id ) ) {
                    $user = get_user_by( 'id', $user_id );
                    $url = get_edit_profile_url( $user_id );
                    printf( '<td><a href="%s" target="_blank">%s</a></td>', $url, $user->user_login );
                } else {
                    printf( '<td>--</td>' );
                }

                // Event
                $event_id = $row['event_id'];
                $url = get_edit_post_link( $event_id );
                $title = $row['event_name'];
                $date = tribe_get_start_date( $event_id, false, 'F d, Y' );
                printf( '<td><a href="%s" target="_blank">%s</a></td>', $url, $title . ' - ' . $date );


                printf( '<td>%s</td>', $row['email'] );
                printf( '<td>%s</td>', $row['phone-number'] );
                printf( '<td>%s</td>', $row['company'] );
                printf( '<td>%s</td>', $row['attendees-first-name'] );
                printf( '<td>%s</td>', $row['attendees-last-name'] );
                printf( '<td>%s</td>', $row['order_status'] );
                printf( '<td>%s</td>', $row['payment_method'] );
                printf( '<td>%s</td>', '$' . $row['price'] );
                printf( '<td>%s</td>', $row['check_number'] );

                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
        <?php endif; ?>

        <style>
            th, td {
                padding: 5px;
                padding-right: 15px;
            }
        </style>

        <?php
    }

    public function save() {

        $output_type = $_POST['_report_output'];
        $attendee_key = $_POST['_attendee_name'];
        $attendees = $this->get_attendees_optimized();
        if ( ! isset( $attendees[$attendee_key] ) ) {
            die('Error: ' . $attendee_key . ' not found...');
        }

        // Get data.
        $data = $attendees[$attendee_key];

        foreach( $data as &$datum ) {
            $order_id = $datum['order_id'];
            $ticket_id = $datum['ticket_id'];
            $order = wc_get_order( $order_id );
            $event = tribe_events_get_ticket_event( $ticket_id );
            $ticket = wc_get_product( $ticket_id );
            $datum['event_id'] = $event->ID;
            $datum['event_name'] = $event->post_title;
            $datum['ticket_name'] = get_the_title( $ticket_id );
            $datum['date_created'] = $order->get_date_created()->format('F d, Y');
            $datum['date_created_timestamp'] = $order->get_date_created()->getTimestamp();
            $datum['user_id'] = $order->get_user_id();
            $datum['price'] = $ticket->get_price();
            $datum['payment_method'] = $order->get_payment_method_title();
            $datum['order_status'] = ucfirst( $order->get_status() );
            $datum['check_number'] = get_post_meta( $order_id, '_check_number', true );
        }

        usort( $data, function( $a, $b ) {
            return $b['date_created_timestamp'] - $a['date_created_timestamp'];
        });

        if ( 'CSV' === $output_type ) {
            $this->output_csv( $data );
            exit;
        } else {
            $this->results = $data;
        }
    }

    protected $results = [];

    public function output_csv( $items = [] ) {

        header( 'Content-Type: text/csv; charset=utf-8' );
        header( 'Content-Disposition: attachment; filename=data.csv' );

        $headers = [
            'Order ID',
            'Order Date',
            'User ID',
            'Email',
            'First Name',
            'Last Name',
            'Event',
            'Ticket',
            'Price',
            'Payment Method',
            'Check Number',
            'Status',
        ];

        foreach( $headers as $header ) {
            printf( '"%s",', $header );
        }
        echo "\n";

        foreach( $items as $item ) {

            printf( '"%s",', $item['order_id'] );
            printf( '"%s",', $item['date_created'] );
            printf( '"%s",', $item['user_id'] );

            printf( '"%s",', $item['email'] );
            printf( '"%s",', $item['attendees-first-name'] );
            printf( '"%s",', $item['attendees-last-name'] );

            printf( '"%s",', $item['event_name'] );
            printf( '"%s",', $item['ticket_name'] );
            printf( '"%s",', $item['price'] );

            printf( '"%s",', $item['payment_method'] );
            printf( '"%s",', $item['check_number'] );


            printf( '"%s",', $item['order_status'] );
            echo "\n";
        }
    }
}