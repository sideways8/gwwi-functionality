<?php
namespace GWWI\Components\Reports\Admin\Tabs;

abstract class ReportSettingsPageTabAbstract
{
    public $key = '';

    public $label = '';

    public function render() {
        echo 'Overwrite render()...';
    }

    public function save() { }

    ////////////////////////

    public $admin_page_slug = 'gwwi-reports';

    public function __construct() {
        add_filter( 'reports_admin_settings_page_tabs', [$this, '_hook_settings_page_tabs'], 10, 2 );
        add_action( 'reports_admin_settings_page_render_tab', [$this, '_hook_settings_page_render'], 10, 2 );
        add_action( 'reports_admin_settings_page_save_tab', [$this, '_hook_settings_page_save'], 10, 2 );
    }

    public function check_hook( $tab, $slug ) {
        if ( ! isset( $_GET['page'] ) || $slug !== $_GET['page'] ) {
            return false;
        }
        if ( $tab !== $this->key || $slug !== $this->admin_page_slug ) {
            return false;
        }
        if ( isset( $_GET['tab'] ) && $_GET['tab'] != $tab ) {
            return false;
        }
        return true;
    }

    public function _hook_settings_page_save( $tab, $slug ) {
        if ( ! $this->check_hook( $tab, $slug ) ) {
            return;
        }
        $this->save();
    }

    public function _hook_settings_page_render( $tab, $slug ) {
        if ( ! $this->check_hook( $tab, $slug ) ) {
            return;
        }
        $this->render();
    }

    public function _hook_settings_page_tabs( $tabs, $slug ) {
        if ( ! isset( $_GET['page'] ) || $slug !== $this->admin_page_slug ) {
            return null;
        }
        $tabs[$this->key] = $this->label;
        return $tabs;
    }
}