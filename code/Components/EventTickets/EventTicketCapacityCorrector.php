<?php
namespace GWWI\Components\EventTickets;

/**
 * This class automatically adjusts ticket capacities when a ticket is refunded.
 *
 * Class EventTicketCapacityCorrector
 *
 * @package GWWI\Components\EventTickets
 */
class EventTicketCapacityCorrector
{
    /**
     * @var \WC_Order
     */
    protected $order;

    public function __construct() {
        add_action( 'woocommerce_order_refunded', [$this, 'maybe_refund_order'], 10, 2 );
        add_action( 'woocommerce_cancelled_order', [$this, 'maybe_refund_order'], 10, 2 );
    }

    /**
     * Loop through order items and increase ticket capacity.
     * @param $order_id int
     * @param $refund_id int
     */
    public function maybe_refund_order( $order_id, $refund_id ) {
        $this->order = wc_get_order( $order_id );
        $order = $this->order;
        foreach( $order->get_items() as $order_item ) {
            /** @var $order_item \WC_Order_Item_Product */
            $product_id = $order_item->get_product_id();
            $this->maybe_adjust_ticket_capacity( $product_id );
        }
    }

    /**
     * If a valid ticket ID is provided, increment the ticket capacity.
     * @param $product_id int
     */
    public function maybe_adjust_ticket_capacity( $product_id ) {
        if ( ! tribe_events_product_is_ticket( $product_id ) ) {
            return;
        }
        $this->increment_ticket_capacity( $product_id );
        $this->maybe_adjust_ticket_stock( $product_id );
    }

    public function maybe_adjust_ticket_stock( $product_id ) {
        $product = wc_get_product( $product_id );
        $stock = $product->get_stock_quantity();
        $stock++;
        // Note $product->set_stock_quantity() is doing nothing, so we are updating stock directly.
        update_post_meta( $product_id, '_stock', $stock );
        $this->order->add_order_note("TICKET: Increasing stock to: {$stock}");
    }

    /**
     * @param $ticket_id int The ticket post ID.
     */
    public function increment_ticket_capacity( $ticket_id ) {
        $ticket_capacity = tribe_tickets_get_capacity( $ticket_id );
        $ticket_capacity++;
        tribe_tickets_update_capacity( $ticket_id, $ticket_capacity );
        $this->order->add_order_note( "TICKET: Increasing ticket capacity to: {$ticket_capacity}" );
    }
}