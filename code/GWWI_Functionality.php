<?php
namespace GWWI;

class GWWI_Functionality
{
    protected function __construct() {
        new Components\Woocommerce\WoocommerceComponent;
        new Components\Reports\ReportsComponent;
        new Components\EventTickets\EventTicketsComponent;
        new Components\Events\EventsComponent;

        add_action( 'admin_enqueue_scripts', [$this, 'enqueue_admin_scripts'] );
    }

    /** @var static */
    protected static $instance;

    /** @var static */
    public static function get_instance() {
        if ( null === static::$instance ) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    public function enqueue_admin_scripts() {
        wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css' );
        wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', ['jquery'] );

        wp_enqueue_style( 'gwwi-functionality-admin', GWWI_PLUGIN_URL . 'assets/css/gwwi-functionality-admin.css', array(), time() );
	    wp_enqueue_script(
		    'wp-media-picker-jquery',
		    GWWI_PLUGIN_URL . 'assets/js/wp-media-picker-jquery.js',
		    array( 'jquery' ),
		    time(),
		    true
	    );
	    wp_enqueue_script(
		    'woocommerce-order',
		    GWWI_PLUGIN_URL . 'assets/js/woocommerce-order.js',
		    array( 'jquery' ),
		    time(),
		    true
	    );
    }
}